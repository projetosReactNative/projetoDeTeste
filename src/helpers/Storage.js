import { AsyncStorage } from 'react-native'

export function gravar(keys){
    AsyncStorage.multiSet(keys)
    .then(()=>{
        console.log('gravou')
        return true
    })
    .catch(()=>{
        console.log('nao gravou')
        return false
    })
}

export async function gravarObjeto(nomeDoObjeto, objeto){
    return await new Promise((resolve, rejected) => {
        let data = JSON.stringify(objeto)
        console.log(data)
        AsyncStorage.setItem(nomeDoObjeto, data)
        .then(response => {
            resolve(response)
        })
        .catch(error => {
            rejected(error)
        })
    })
}

export async function removerTodasChaves(){
    return await new Promise((resolve, rejected) => {
        AsyncStorage.getAllKeys((error, keys) => {
            console.log(keys)
            AsyncStorage.multiRemove(keys, error => {
                console.log(error)
                if(error === null){
                    resolve(response = {status: true, message: 'Todas as chaves foram removidas'})
                } else {
                    rejected(error={status: false, message: 'Erro ao remover chaves'})
                }
            });
        })
    })
}

export async function recuperarObjeto(nomeDoObjeto){
    return await new Promise((resolve, rejected) => {
        AsyncStorage.getItem(nomeDoObjeto)
        .then(response => {
            resolve(JSON.parse(response))
        })
        .catch(error => {
            rejected(error)
        })
    })
}


export async function recuperar(keys){
    return await new Promise((resolve, rejected) => {
        let data = {}
        let error = []
        AsyncStorage.multiGet(keys, (errors, result) => {
            console.log(result)
            for( let i = 0; i < result.length; i++){
                if(result[i][1] !== null){
                    result[i][0] = result[i][1]
                } else {
                    error.push(result[i][0])
                }
            }
            response = {
                status: true,
                data: {
                    usuario: result[0][1],
                    senha: result[1][1]
                },
                error: error 
            }
            resolve(response);
        })  
    })
}

export async function recuperarDadosLogin(keys){
    return await new Promise((resolve, rejected) => {
        AsyncStorage.multiGet(keys, (errors, result) => {
            if(result[0][1] !== null && result[1][1] !== null){
                console.log(result[1][1])
                response= {
                    status: true,
                    data: {
                        usuario: result[0][1],
                        senha: result[1][1]
                    }
                }
                resolve(response);
            } else {
                rejected({
                    status: false,
                    message: 'Não foi encontrado dados para essas chaves'
                })
            }
        })
    })
}
