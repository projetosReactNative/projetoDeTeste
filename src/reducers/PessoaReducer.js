const INITIAL_STATE = {
    pessoa: {},
    empresaParaMovimentacao: {}
};

export default (state = INITIAL_STATE, action) => {
switch(action.type){
    case 'SET_PESSOA':
        console.log(action.payload)
        return { ...state, pessoa: action.payload}
    case 'SET_EMPRESA_MOVIMENTACAO':
        console.log(action.payload)
        return { ...state, empresaParaMovimentacao: action.payload}
    default: 
        return (state);
}


};
