const INITIAL_STATE = {
        autenticacaoResultado: {}
    };

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case 'SET_AUTENTICACAO_RESULTADO':
            return { ...state, autenticacaoResultado: action.payload}
        default: 
            return (state);
    }

    
};
