import { combineReducers } from 'redux';
import AutenticacaoReducer from './AutenticacaoReducer';
import PessoaReducer from './PessoaReducer';

export default combineReducers({
    PessoaReducer,
    AutenticacaoReducer
});