import { Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'

import { gravar, gravarObjeto } from '../helpers/Storage'

baseURL = 'http://minhaempresa.zyro.com.br'

export const setEmpresaMovimentacao = (empresa) => 
dispatch => {
    gravarObjeto('empresaSelecionada', empresa)
    .then(response => {
        console.log(response)
        dispatch ({
            type: 'SET_EMPRESA_MOVIMENTACAO',
            payload: empresa
        })
        console.log('passei aqui')
        Actions.inicioScreen()
    })
}

export const setPessoa = (usuarioId, token) => dispatch =>{
    console.log('dispatch pessoa ok')
    fetch(baseURL + '/api/pessoas/usuario/' + usuarioId, {
        method: 'get',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: token
        }
    }).then(response => {
        dispatch({
            type: 'SET_PESSOA',
            payload: JSON.parse(response._bodyInit)
        })
    }).catch()
}

export async function verificaDesbloqueado(usuarioId, token){
    return await new Promise((resolve, rejected) => {
        fetch(baseURL + '/api/pessoas/usuario/' + usuarioId, {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
            }
        }).then(resp => {
            let response = false
            if(JSON.parse(resp._bodyInit).status == true){
                response = true
            } else {
                response = false
            }
            resolve(response)
        }).catch(erro => {
            rejected(false)
        })
    })
}