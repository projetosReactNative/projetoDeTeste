import { Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'

import { gravar, recuperarObjeto} from '../helpers/Storage'
import { verificaDesbloqueado, setEmpresaMovimentacao, setPessoa } from '../actions/PessoaActions'

baseURL = 'http://minhaempresa.zyro.com.br'

const setAutenticacaoResultado = (data) => ({
    type: 'SET_AUTENTICACAO_RESULTADO',
    payload: data
})

export const login = (data, firstLogin) => 
dispatch => {
    fetch( `${baseURL}/api/autenticacao/login`, {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      }).then((resp) => {
            const response = JSON.parse(resp._bodyInit)
            if(resp.status == 400){
                Alert.alert('Login', response)
            } else {
                verificaDesbloqueado(response.usuarioId, 'Bearer ' + response.token)
                .then(resp => {
                    if(resp == true){
                        gravar([['usuarioId',response.usuarioId],['token','Bearer ' + response.token]])
                        gravar([['usuario',data.usuario],['senha',data.senha]])

                        dispatch([
                            setAutenticacaoResultado(response),
                            setPessoa(response.usuarioId, 'Bearer ' + response.token)
                        ])                    
                        if(response.tenants.length > 1 && firstLogin == true){
                            Actions.empresaScreen();
                        } else if(firstLogin == true){
                            Actions.inicioScreen();
                        } else {
                            recuperarObjeto('empresaSelecionada')
                            .then(response => {
                                console.log(response)
                                dispatch([setEmpresaMovimentacao(response)])
                            })
                            console.log('carregou pelo pessoa actions')
                        }
                    } else {
                        Alert.alert('Login','Usuário está bloqueado para realizar login')
                        if(!firstLogin){
                            Actions.loginScreen();
                        }
                    }
                })
                .catch(erro => {
                    console.log(erro)
                    Alert.alert('Login','Este usuário não está habilitado para acessar o aplicativo')
                    if(!firstLogin){
                        Actions.loginScreen();
                    }
                })
            }           
      }).catch((erro) => {
            console.log(erro)
            Alert.alert('Internet', 'Verifique sua conexão com a internet');
            if(!firstLogin){
                Actions.loginScreen();
            }
      });
};

