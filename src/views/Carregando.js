
import React, { Component } from 'react';
import { Text, Image, Dimensions, StyleSheet, View, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux';

import { login } from '../actions/AutenticacaoActions';
import { recuperarDadosLogin, recuperarObjeto} from '../helpers/Storage'

class Carregando extends Component {

    componentWillMount(){
        recuperarDadosLogin(['usuario','senha'])
        .then(response => {
            console.log(response)            
            if (response.status){
                this.props.login(response.data, false)
            }  
        })
        .catch(reason => {
            if(!reason.status){
                Actions.loginScreen()
            }
            console.log(reason)
        })
    }

    render() {
        return (
            <View style={styles.app}> 
                <Text>Carregando</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    app: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#c2ffd2',
    },
    content: {
      padding: 16,
      backgroundColor: '#c2ffd2',
      borderRadius: 8,
    },
    arrow: {
      borderTopColor: '#c2ffd2',
    },
    background: {
      backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
  });

// const mapStateToProps = state => (
//     {
//     }
//   );

const mapActionsToProps = {
    login
}

export default connect(null, mapActionsToProps)(Carregando);
