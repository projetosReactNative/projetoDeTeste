import React, { Component } from 'react';
import { Text, Image, Dimensions, StyleSheet, View, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { setEmpresaMovimentacao } from '../actions/PessoaActions'

class Empresas extends Component {

    componentWillMount(){
      console.log(this.props.Autenticacao)
    }

    render() {
        return (
            <View style={styles.app}> 
                <Text style={{paddingHorizontal: 20, paddingVertical: 10}} >Selecione a empresa</Text>
                {
                  this.props.Autenticacao.tenants
                  .map((item, i) => {
                    return(
                      <View key={i} style={{flexDirection: 'row'}}>
                        <TouchableOpacity 
                          style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'rgba(255,255,255,0.8)'}} 
                          onPress={ () => { console.log(item); this.props.setEmpresaMovimentacao(item)}}
                        >
                          <Text>Nome: {item.nome}</Text>
                        </TouchableOpacity>
                      </View>
                    )
                  })
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    app: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#c2ffd2',
    },
    content: {
      padding: 16,
      backgroundColor: '#c2ffd2',
      borderRadius: 8,
    },
    arrow: {
      borderTopColor: '#c2ffd2',
    },
    background: {
      backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
  });

const mapStateToProps = state => (
  {
      Autenticacao : state.AutenticacaoReducer.autenticacaoResultado
  }
);

const mapActionsToProps = {
  setEmpresaMovimentacao
}

export default connect(mapStateToProps, mapActionsToProps)(Empresas);
