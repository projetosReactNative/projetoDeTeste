
import React, { Component } from 'react';
import { Text, Image, Dimensions, StyleSheet, View, TextInput, TouchableOpacity, AsyncStorage} from 'react-native';
import { connect } from 'react-redux';

import { login } from '../actions/AutenticacaoActions'

class Login extends Component {

    constructor(){
      super()
      this.state={
        email: 'pedro.paulods@hotmail.com',
        senha: 'Firewall@123'
      }
    }

    componentWillMount(){
      // this.props.login({usuario:'pedro.paulods@hotmail.com', senha:'Firewall@123'})
    }

    verificaChaves(){
      AsyncStorage.getAllKeys((error, keys) => {
        console.log(keys)
      });
    }

    render() {
        return (
            <View style={styles.app}> 
                <Text>Tela de login</Text>
                <TextInput value={this.state.email} onChangeText={(text) => {this.setState({email: text})}} />
                <TextInput value={this.state.senha} onChangeText={(text) => {this.setState({senha: text})}} />
                <TouchableOpacity style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'rgba(0,0,0,0.3)'}} onPress={()=>{this.props.login({usuario: this.state.email, senha: this.state.senha}, true)}} >
                    <Text>Entrar</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'rgba(0,0,0,0.3)'}} onPress={()=>{this.verificaChaves()}} >
                    <Text>Verificar chaves </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    app: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#c2ffd2',
    },
    content: {
      padding: 16,
      backgroundColor: '#c2ffd2',
      borderRadius: 8,
    },
    arrow: {
      borderTopColor: '#c2ffd2',
    },
    background: {
      backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
  });

// const mapStateToProps = state => (
//     {
//     }
//   );

const mapActionsToProps = {
  login
}

export default connect(null, mapActionsToProps)(Login);
