
import React, { Component } from 'react';
import { Text, Image, Dimensions, StyleSheet, View, AsyncStorage, TouchableOpacity} from 'react-native';
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux';

import { removerTodasChaves } from '../helpers/Storage'

class Inicio extends Component {

    componentDidMount(){
        console.log(this.props.Autenticacao)
        console.log(this.props.Pessoa)
    }

    logout(){
        removerTodasChaves()
        .then(response => {
            console.log(response);
            response.status && Actions.loginScreen()
        })
        .catch(error => {
            console.log(error)
        })
    }

    render() {
        return (
            <View style={styles.app}> 
                <Text>Inicio</Text>
                <TouchableOpacity style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'rgba(0,0,0,0.3)'}} onPress={()=>{console.log(this.props.Pessoa)}} >
                    <Text>Print pessoa</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{paddingHorizontal: 20, paddingVertical: 10, backgroundColor: 'rgba(0,0,0,0.3)'}} onPress={()=>{this.logout()}} >
                    <Text>Sair</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    app: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#c2ffd2',
    },
    content: {
      padding: 16,
      backgroundColor: '#c2ffd2',
      borderRadius: 8,
    },
    arrow: {
      borderTopColor: '#c2ffd2',
    },
    background: {
      backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
  });

const mapStateToProps = state => (
    {
        Autenticacao : state.AutenticacaoReducer,
        Pessoa: state.PessoaReducer
    }
  );

export default connect(mapStateToProps, null)(Inicio);
