
import React, { Component } from 'react';
import { Text, Image, Dimensions, StyleSheet } from 'react-native';
import { Router, Scene, Stack, Actions, Drawer } from 'react-native-router-flux';
import { connect } from 'react-redux';

import Login from './views/Login'
import Carregando from './views/Carregando'
import Empresas from './views/Empresas'
import Inicio from './views/Inicio'

class Routes extends Component {
    render() {
        return (
                    <Router>
                        <Scene key='root' hideNavBar >
                            <Scene key='carregandoScreen' navigationBarStyle={{backgroundColor: '#1AB394'}} component={Carregando} title="Login" hideNavBar />
                            <Scene key='loginScreen' navigationBarStyle={{backgroundColor: '#1AB394'}} component={Login} title="Login" hideNavBar />
                            <Scene key='empresaScreen' navigationBarStyle={{backgroundColor: '#1AB394'}} component={Empresas} title="Login" hideNavBar />
                            <Scene key='inicioScreen' navigationBarStyle={{backgroundColor: '#1AB394'}} component={Inicio} title="Login" hideNavBar />
                        </Scene>
                    </Router>
                );
    }
}

const styles = StyleSheet.create({
    app: {
      ...StyleSheet.absoluteFillObject,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#c2ffd2',
    },
    content: {
      padding: 16,
      backgroundColor: '#c2ffd2',
      borderRadius: 8,
    },
    arrow: {
      borderTopColor: '#c2ffd2',
    },
    background: {
      backgroundColor: 'rgba(255, 255, 255, 0.5)'
    },
  });

// const mapStateToProps = state => (
//     {
//     }
//   );

export default connect(null, null)(Routes);
